
# Netsuite Data Source Setup

## Step 1 – Enable the Connect Service feature

1. 	Ensure that Account and Role were enabled with the Connect Service feature by your Account Administrator your Account Administrator has enabled your Account and Role with the Connect Service feature.

2. 	Navigate to *Setup > Company > Enable Features*.

3. 	Click the **Analytics** tab.

5. 	Check the **SuiteAnalytics Connect** box.
*Note: If you do not see this feature, it has not been provisioned for your account. Contact NetSuite Customer Support or your Account Manager for assistance.*

Go back to the “Home” page

5. Navigate to *Settings > Set Up Suite > Analytics Connect*.

7. You should see your configuration summary, copy and save it for later steps when connecting Netsuite in Datricks dashboard.
--Picture--

## Step 2 – Enable the Connect Service feature

1.  Navigate to *Setup > Company > Enable Features*.

2.  Click the **SuiteCloud** tab.

3.  Make sure the following checkboxes are selected:

	- SOAP WEB SERVICES
	- REST WEB SERVICES
	- TOKEN-BASED AUTHENTICATION


## Step 3 – Install Datricks Connector bundle

   1. 	Navigate to _Customization > SuiteBundler > Search & Install bundles_.

   2. 	Enter the bundle number **396954** and click on **search**.

   3. 	Select **'Datricks Connector**' and click on **install** > **install bundles**.


## Step 4 – Create a user
Create a new Netsuite user for the connection with Datricks.

Make sure that:
* It has a **valid** and **unexpired** password.
* The option: ‘**_Require Password Change on Next Login_**’ on the Employee record’s access tab is not selected.
* The **employee record** is active.

## Step 5 – Install Datricks Connector bundle
A new “Datricks Connector” role was imported during the bundle installation.

To allow editing without overridden, copy this “Datricks Connector” to a new role and assign it to the user created in step 4.

## Questions:
Why steps 1 and 2 have the same title?
